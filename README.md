# bixi-data-analytic

Data analysis project done against the clock, with a makeshift web dashboard. Everything self-contained.

# Installation
It's docker based so, all you need is internet connection and docker installed.

# Executing

**Using Dockerfile:**

Open a command line into the folder where Dockerfile is present.

Execute the following command to build our container:

`docker build -t bixi-analytic .`

After the success build, run following command to run our application:

`docker run -p 80:80 --name bixi bixi-analytic`

**Running it with a regular python interpreter:**

Run following command to install the requirements:

`pip install -r requirements.txt`

Run following command to run the application:

`python app/app.py`

# Using the application

Just go to your browser and go to:

`http://127.0.0.1:5000`

In case it's working in an external server, you must replace the 127.0.0.1 with the ip of the server
