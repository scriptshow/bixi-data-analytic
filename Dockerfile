FROM python:3.7

# change work directory
RUN mkdir -p /bixi-data-analytic
WORKDIR /bixi-data-analytic

ADD ./ /bixi-data-analytic/
RUN pip install -r requirements.txt

EXPOSE 80

# Comment this line once tests done
ENTRYPOINT ["./gunicorn_starter.sh"]
