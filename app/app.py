import io
from flask import Flask, render_template, Response
from core.histogram import create_figure
from core.stations import get_top_stations, get_stations_name
from core.travels import get_top_travels
from core.hours import get_busy_hours
from core.utilization import get_utilization

app = Flask(__name__, static_url_path='', static_folder='static', template_folder='templates')


@app.route('/histogram_<year>.png')
def plot_png(year):
    fig = create_figure(year)
    output = io.BytesIO()
    fig.savefig(output, format='png')
    fig.clear()
    return Response(output.getvalue(), mimetype='image/png')

@app.route("/histogram/<year>")
def histogram(year):
    templateData = {
        'year': year
    }
    return render_template('histogram.html', **templateData)

@app.route("/stations/top/<number>/<year>")
def stations(number, year):
    start, end, mix = get_top_stations(year, int(number))
    start_names = get_stations_name(year, start)
    end_names = get_stations_name(year, end)
    mix_names = get_stations_name(year, mix)
    templateData = {
        'start': [start_names[code] for code in start if code in start_names],
        'end': [end_names[code] for code in end if code in end_names],
        'mix': [mix_names[code] for code in mix if code in mix_names],
        'top': number,
        'year': year
    }
    return render_template('stations.html', **templateData)

@app.route("/travels/top/<number>/<year>")
def travels(number, year):
    mix_readable = []
    mix_codes = get_top_travels(year, int(number))
    for two_set in mix_codes:
        set_data = get_stations_name(year, two_set)
        mix_readable.append(f'{set_data[two_set[0]]} to {set_data[two_set[1]]}')

    templateData = {
        'mix': mix_readable,
        'top': number,
        'year': year
    }
    return render_template('travels.html', **templateData)

@app.route("/busy-hour/<year>")
def busy_hours(year):
    result = get_busy_hours(year)
    templateData = {
        'hours': result,
        'year': year
    }
    return render_template('busy-hour.html', **templateData)

@app.route("/utilization/<year1>/<year2>")
def utilization(year1, year2):
    travels_first, total_time_first, travels_per_bike_first, travels_per_station_first = get_utilization(year1)
    travels_last, total_time_last, travels_per_bike_last, travels_per_station_last = get_utilization(year2)
    templateData = {
        'year_first': year1,
        'travels_first': travels_first,
        'total_time_first': total_time_first,
        'travels_per_bike_first': travels_per_bike_first,
        'travels_per_station_first': travels_per_station_first,
        'year_last': year2,
        'travels_last': travels_last,
        'total_time_last': total_time_last,
        'travels_per_bike_last': travels_per_bike_last,
        'travels_per_station_last': travels_per_station_last
    }
    return render_template('utilization.html', **templateData)

# main route
@app.route("/")
def index():
    return render_template('main.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=False)
