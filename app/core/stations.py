import pandas as pd
from os import path


def get_top_stations(year, number):
    """
    Function to take the top stations used in a year
    """
    if path.isfile(f"../data/OD_{year}.csv") and number > 0:
        data = pd.read_csv(f"../data/OD_{year}.csv", usecols=['start_station_code', 'end_station_code'])

        result_start = data['start_station_code'].value_counts()[:number].index.tolist()
        result_end = data['end_station_code'].value_counts()[:number].index.tolist()
        result_mix = pd.concat([data['start_station_code'], data['end_station_code']]).value_counts()[:number].index.tolist()

        return result_start, result_end, result_mix
    else:
        return None, None, None


def get_stations_name(year, codes):
    """
    Function to take the human readable name from stations codes by year
    """
    result = {}
    if path.isfile(f"../data/Stations_{year}.csv"):
        data = pd.read_csv(f"../data/Stations_{year}.csv", usecols=['code', 'name'])

        for code in codes:
            try:
                data3 = data.loc[data['code'] == int(code), 'name'].item()

                result[code] = data3
            except ValueError:
                pass

    return result
