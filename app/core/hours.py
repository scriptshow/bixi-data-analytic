import pandas as pd
from os import path


def get_busy_hours(year):
    """
    Function to get busy hours for a full year
    """
    if path.isfile(f"../data/OD_{year}.csv"):
        data = pd.read_csv(f"../data/OD_{year}.csv", usecols=['start_date', 'end_date'])

        data[['start_day', 'start_hour_min']] = data['start_date'].str.split(expand=True)
        data[['start_hour', 'start_min']] = data['start_hour_min'].str.split(':', expand=True)

        result = data['start_hour'].value_counts()

        return result[result > ((result.max() + result.mean()) / 2)].index.tolist()
    else:
        return None
