import matplotlib.pyplot as plot
import pandas as pd
from os import path


def load_histogram(year):
    """
    Function to retrieve the histogram for a year
    """
    if path.isfile(f"../data/OD_{year}.csv"):
        durations = pd.read_csv(f"../data/OD_{year}.csv", usecols=['duration_sec', ])['duration_sec'].values.tolist()

        data = pd.Series(durations)

        data_plot = data.plot.hist(bins=100, color='#ffcc66')
        plot.xlabel('Duration')
        plot.title('Histogram')

        return data_plot
    else:
        return None


def create_figure(year):
    plot = load_histogram(year)
    fig = plot.get_figure()
    return fig
