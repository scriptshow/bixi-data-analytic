import pandas as pd
from os import path


def get_top_travels(year, number):
    """
    Function to get the top travels for a year
    """
    if path.isfile(f"../data/OD_{year}.csv"):
        data = pd.read_csv(f"../data/OD_{year}.csv", usecols=['start_station_code', 'end_station_code'])

        result_mix = data.value_counts(subset=['start_station_code', 'end_station_code'])[:number].index.tolist()

        return result_mix
    else:
        return None
