import pandas as pd
from os import path


def get_utilization(year):
    if path.isfile(f"../data/OD_{year}.csv"):
        data = pd.read_csv(f"../data/OD_{year}.csv", usecols=['duration_sec', 'start_station_code'])
        data2 = pd.read_csv(f"../data/Stations_{year}.csv", usecols=['name', ])
        stations_data = pd.read_json(f"../data/stations.json")
        normalized_stations_data = pd.json_normalize(stations_data['stations'])

        travels = len(data.index)
        total_time = data['duration_sec'].sum()
        travels_per_bike = travels / (normalized_stations_data['ba'].sum())
        travels_per_station = travels / data2.index.__len__()

        return travels, total_time, travels_per_bike, travels_per_station
    else:
        return None

