import unittest
import os
from app.core.histogram import load_histogram


class TestHistogram(unittest.TestCase):

    """
    Tests cases for histogram functions
    """

    def test_histogram(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = load_histogram('2014')
        self.assertNotEqual(result, None)

    def test_wrong_year(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = load_histogram('2000')
        self.assertEqual(result, None)


if __name__ == '__main__':
    unittest.main()
