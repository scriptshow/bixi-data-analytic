import unittest
import os
from app.core.utilization import get_utilization


class TestUtilization(unittest.TestCase):

    """
    Tests cases for utilization functions
    """

    def test_histogram(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_utilization('2014')
        self.assertNotEqual(result, None)

    def test_wrong_year(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_utilization('2000')
        self.assertEqual(result, None)


if __name__ == '__main__':
    unittest.main()
