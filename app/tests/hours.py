import unittest
import os
from app.core.hours import get_busy_hours


class TestHours(unittest.TestCase):

    """
    Tests cases for busy hours functions
    """

    def test_hours(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_busy_hours('2014')
        self.assertNotEqual(result, None)

    def test_wrong_year(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_busy_hours('2000')
        self.assertEqual(result, None)


if __name__ == '__main__':
    unittest.main()
