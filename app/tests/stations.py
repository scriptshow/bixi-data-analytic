import unittest
import os
from app.core.stations import get_top_stations, get_stations_name


class TestStations(unittest.TestCase):

    """
    Tests cases for stations functions
    """

    def test_stations(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_top_stations('2014', 5)
        self.assertNotEqual(result, None)

    def test_wrong_year(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_top_stations('2000', 5)
        self.assertEqual(result, (None, None, None))

    def test_wrong_top_number(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_top_stations('2014', -1)
        self.assertEqual(result, (None, None, None))

    def test_station_name(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_stations_name('2014', (6211, 6233))
        self.assertNotEqual(result, {})

    def test_wrong_station_name(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_stations_name('2014', (0, 5))
        self.assertEqual(result, {})


if __name__ == '__main__':
    unittest.main()
