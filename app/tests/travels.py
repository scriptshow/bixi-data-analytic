import unittest
import os
from app.core.travels import get_top_travels


class TestTravels(unittest.TestCase):
    """
    Tests cases for travel functions
    """

    def test_travels(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_top_travels('2014', 5)
        self.assertNotEqual(result, None)

    def test_wrong_year(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_top_travels('2000', 5)
        self.assertEqual(result, None)

    def test_wrong_top_number(self):
        os.chdir(os.getcwd() + os.sep + '..')
        result = get_top_travels('2014', -1)
        self.assertEqual(result, None)


if __name__ == '__main__':
    unittest.main()
